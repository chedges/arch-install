# Arch Install

This repository stores my scripts for installing Arch Linux

The script will check for a hostname in a file called "hostname". If the file is not found, it will prompt for the hostname.

The chroot script will prompt for a first user to create, as well as a password for that user.
