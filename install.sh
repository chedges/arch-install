#!/usr/bin/bash

## This script will run through the process of installing Arch Linux using the Arch live environment
## Based on https://wiki.archlinux.org/index.php/installation_guide

DRIVE=""
HOSTNAME=""
REBOOT="No"

while :; do
    case $1 in
        -d|--drive)
            if [ "$2" ]; then
                DRIVE="$2"
                shift
            else
                echo "Drive flag requires one argument (e.g. /dev/sda)"
                exit 1
            fi
            ;;
        -n|--name)
            if [ "$2" ]; then
                HOSTNAME="$2"
                shift
            else
                echo "Name flag requires one argument to define the hostname"
                exit 1
            fi
            ;;
        -r|--reboot)
            REBOOT="Yes"
            ;;
        --)
            shift
            break
            ;;
        *)
            break
    esac

    shift
done

if [ -z "$DRIVE" ] || [ -z "$HOSTNAME" ]; then
    echo "Both drivename and hostname are required"
    exit 1
fi

DRIVESIMPLE=$(echo "$DRIVE" | cut -d '/' -f 3)

# Update the system clock
timedatectl set-ntp true && echo "Updated system clock"

# Check if we're booted in UEFI or BIOS
if [ -d /sys/firmware/efi/efivars ]; then
  EFIBOOT="Yes"
else
  EFIBOOT="No"
fi

echo "EFI Booted: $EFIBOOT"

# Partition disk based on whether it should be UEFI/GPT or BIOS/MBR
echo "Will partition "$DRIVE""

if [[ "$EFIBOOT" == "No" ]]; then
  parted "$DRIVE" --script mklabel msdos mkpart primary ext4 1MiB 100% set 1 boot on && echo "Successfully partitioned for BIOS/MBR"
else
  parted "$DRIVE" --script mklabel gpt mkpart primary fat32 1MiB 551MiB set 1 esp on mkpart primary ext4 551MiB 100% && echo "Successfully partitioned for UEFI/GPT"
fi

EFIPARTITION=$(fdisk -l | awk '/'"$DRIVESIMPLE"'.*EFI/ { print $1 }')
MAINPARTITION=$(fdisk -l | awk '/'"$DRIVESIMPLE"'.*Linux/ { print $1 }')

# Format the partition
mkfs.ext4 "$MAINPARTITION" && echo "Formatted $MAINPARTITION as ext4"

# Mount the partitions
mount "$MAINPARTITION" /mnt && echo "Mounted $MAINPARTITION to /mnt"

if [[ "$EFIPARTITION" != "" ]]; then
  mount "$EFIPARTITION" /mnt/efi && echo "Mounted $EFIPARTITION to /mnt/efi"
fi

# Setup /etc/pacman.d/mirrorlist
reflector --country "United States" --latest 70  --protocol https --sort rate --save /etc/pacman.d/mirrorlist && echo "Successfully loaded mirrorlist via reflector"

# Install the base packages with pacstrap
pacstrap /mnt base linux linux-firmware

# Generate an fstab
genfstab -U /mnt >> /mnt/etc/fstab && echo "Generated fstab into /mnt/etc/fstab, please check for consistency if there are errors"

# Prepare chroot setup by copying finishchroot.sh into /mnt
cp finishchroot.sh /mnt/finishchroot.sh && echo "Put finishchroot.sh in /mnt/finishchroot.sh"

# Set hostname
echo "$HOSTNAME" > /mnt/etc/hostname && echo "Successfully set /etc/hostname"

# Set /etc/hosts file
echo "127.0.0.1    localhost" >> /mnt/etc/hosts && \
echo "::1          localhost" >> /mnt/etc/hosts && \
echo "127.0.1.1    $HOSTNAME.local $HOSTNAME" >> /mnt/etc/hosts && echo "Successfully set /etc/hosts with hostname $HOSTNAME"

# Copy systemd-networkd config files over
cp /etc/systemd/network/* /mnt/etc/systemd/network/

# Finish setup in chroot
arch-chroot /mnt /finishchroot.sh --efiboot "$EFIBOOT" && echo "Finished chroot install"

# Unmount partition
umount -R /mnt && echo "Successfully unmounted partitions"

if [[ "$REBOOT" == "Yes" ]]; then
    # Eject installation media and reboot into installed system
    eject -m; reboot -f
fi
