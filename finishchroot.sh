#!/bin/bash

## This script will finish up the Arch installation in the chroot

EFIBOOT="No"

while :; do
    case $1 in
        -e|--efiboot)
            if [[ "$2" == "No" ]] || [[ "$2" == "Yes" ]]; then
                EFIBOOT="$2"
                shift
            fi
            ;;
        --)
            shift
            break
            ;;
        *)
            break
    esac

    shift
done

# Set time zone to Los Angeles
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime && echo "Set time zone to America/Los Angeles"

# Sync with hardware clock
hwclock --systohc && echo "Sync with hardware clock"

# Set locale
sed -ibak 's/^#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen && echo "Uncommented en_US.UTF-8 line in /etc/locale.gen"
locale-gen && echo "Successfully generated locales"
echo "LANG=en_US.UTF-8" >> /etc/locale.conf && echo "Successfully set LANG variable in /etc/locale.conf"

# Enable systemd-networkd
systemctl enable systemd-networkd.service && echo "Successfully enabled systemd-networkd"

# Enable systemd-resolved
systemctl enable systemd-resolved.service && echo "Successfully enabled systemd-resolved"

# Install sudo for later
pacman -S --noconfirm sudo && echo "Successfully installed sudo"

# Set wheel group for sudo access
sed -ibak 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /etc/sudoers

# Set root password
echo "Set root password"
passwd && echo "Root password set"

# Install ansible, ssh, git, nano, and base-devel for finishing setup with first user
pacman -S --noconfirm ansible openssh base-devel git nano

# Install Intel microcode updates
pacman -S --noconfirm intel-ucode && echo "Installed Intel Microcode"

MAINDISK=$(fdisk -l | grep Linux | awk {'print $1'} | rev | cut -c 2- | rev)

# Install grub ## add efi flag for grub stuff
if [[ "$EFIBOOT" == "Yes" ]]; then
    pacman -S --noconfirm grub efibootmgr && echo "Successfully installed grub and efibootmgr"
    grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --recheck && echo "Successfully installed grub to /boot/efi"
else
    pacman -S --noconfirm grub && echo "Successfully installed grub"
    grub-install "$MAINDISK" && echo "Successfully installed grub to $MAINDISK"
fi
grub-mkconfig -o /boot/grub/grub.cfg && echo "Successfully set grub config"

# Create first user
read -p 'First user: ' USERNAME
useradd -m -G wheel "$USERNAME" && echo "Successfully created user $USERNAME"
passwd "$USERNAME" && echo "Successfully set password for $USERNAME"

# Exit the chroot
exit

